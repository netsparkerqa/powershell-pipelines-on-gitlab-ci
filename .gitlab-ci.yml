stages:
  - run_on_windows_with_artifact_gen
  - run_on_windows
  - run_on_linux_docker_pwsh_preinstalled
  - run_on_linux_docker_pwsh_runtime_installed

yml_embedded_script_on_windows:
  stage: run_on_windows_with_artifact_gen
  # There is no image: tag because Windows shared runners are shell runners
  # These tags are only required for targeting shared Windows runners on GitLab.com, remove for self-hosted private runners
  tags:  
  - shared-windows
  - windows
  - windows-1809

  variables:
    SCENARIO: "*** SCENARIO: Run inline .gitlab-ci.yml PowerShell code on a Windows Shell runner"

  script:
    - |
      Write-Host "$SCENARIO"
      If ((!(Test-Path variable:IsWindows)) -OR ($IsWindows)) {
        #IsWindows does not exist in Windows PowerShell (first check above) and is $True on PowerShell Core / 7 on Windows (second check above)
        $CurrentUser = ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent())
        $IsAdmin = $CurrentUser.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)
        write-host "This session has elevated Administrator rights => $IsAdmin"
        write-host "Elevated admin is ok because this virtual machine is only ever used for one job and tossed - like a container."
      } else {
        write-host "Not running on Windows."
      }
      write-host "This script is inline in the .gitlab-ci.yml"
      write-host "When shell code runs on Windows from an inline script, the default scripting language is PowerShell"      
      write-host "Running in project $env:CI_PROJECT_NAME with results at $env:CI_JOB_URL ($env:CI_JOB_URL)."
      write-host "PowerShell Version Details:"
      $PSVersiontable      
      write-host "GitLab CI Variables are always propagated to PowerShell as environment variables and ONLY FOR the default PowerShell shell they are also propagated to regular PowerShell variables."
      write-host "Listing all PowerShell variables:"
      dir variable: | format-table Name,Value
      write-host "Listing all Environment variables:"
      dir env: | format-table Name,Value
      set-content -path myartifactfile.txt -value "This artifact was built on PowerShell for Windows in a job named yml_embedded_script_on_windows"
  
  artifacts:
    paths: [ myartifactfile.txt ]

repository_based_script_on_windows:
  stage: run_on_windows
  # There is no image: tag because Windows shared runners are shell runners
  # These tags are only required for targeting shared Windows runners on GitLab.com, remove for self-hosted private runners
  tags:  
  - shared-windows
  - windows
  - windows-1809

  variables:
    SCENARIO: "*** SCENARIO: Run PowerShell code stored as a script file in the repo on a Windows Shell runner"

  script: ./repository-based-script.ps1

powershell_core_on_windows:
  stage: run_on_windows
  # There is no image: tag because Windows shared runners are shell runners
  # These tags are only required for targeting shared Windows runners on GitLab.com, remove for self-hosted private runners
  tags:  
  - shared-windows
  - windows
  - windows-1809

  variables:
    SCENARIO: "*** SCENARIO: Install PowerShell Core on Windows and run PowerShell code stored as a script file"

  script:
    - |
      # Chocolatey is already installed on GitLab.com Shared Windows Runners, if you are running on a machine that does not have it, run this oneliner instead:
      # If (!(Test-Path env:chocolateyinstall)) {iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex} ; cinst -y powershell-core 
      chocolatey install -y powershell-core
      $env:Path = [System.Environment]::ExpandEnvironmentVariables([System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User"))
      write-host "Using the newly installed PowerShell Core to run the repository script..."
      pwsh -f ./repository-based-script.ps1

yml_embedded_script_on_linux_pwsh_preinstalled:
  stage: run_on_linux_docker_pwsh_preinstalled
  image: mcr.microsoft.com/dotnet/core/sdk:3.1
  #Checkout other available Microsoft containers: https://hub.docker.com/_/microsoft-dotnet-core-sdk/

  variables:
    SCENARIO: "*** SCENARIO: Run PowerShell code stored as a script file in the repo on Microsoft's .NET Core SDK Linux docker image"

  script:
    - |
      echo "$SCENARIO"
      echo "The default shell here is bash"
      pwsh -c 'Write-Host "However, powershell is preinstalled and callable on this Microsoft .NET Core SDK Image like this"'
      pwsh -c 'Write-Host "Since repository-based-script.ps1 does not have any windows dependencies, we can also call it here:"'
      pwsh -f ./repository-based-script.ps1
      pwsh -c 'Write-Host "We can also manipulate the artifact generated in a Windows job in this same pipeline:"'
      pwsh -c "Get-Content myartifactfile.txt"

yml_embedded_script_on_linux_pwsh_runtime_installed:
  stage: run_on_linux_docker_pwsh_runtime_installed
  image: amazonlinux
  #Checkout other available Microsoft containers: https://hub.docker.com/_/microsoft-dotnet-core-sdk/

  variables:
    SCENARIO: "*** SCENARIO: Install PowerShell Core on Amazon Linux 2 and run PowerShell code stored as a script file"

  script:
    - |
      echo "$SCENARIO"
      echo "The default shell here is bash"
      echo "Let's install the latest PowerShell for this platform"
      curl -s https://raw.githubusercontent.com/PowerShell/PowerShell/master/tools/installpsh-amazonlinux.sh -o installpsh-amazonlinux.sh
      . ./installpsh-amazonlinux.sh
      echo "PowerShell installation is complete"
      pwsh -c 'Write-Host "Now that we have installed powershell it is callable like this"'
      pwsh -f ./repository-based-script.ps1
      pwsh -c 'Write-Host "We can also manipulate the artifact generated in a Windows job in this same pipeline:"'
      pwsh -c "Get-Content myartifactfile.txt"      
